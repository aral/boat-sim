
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path')
  , io = require('socket.io')
  , fs = require('fs');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', function(req, res){
	res.sendfile("views/index.html");
});
app.get('/users', user.list);

var server = http.createServer(app);
var socket_io = io.listen(server);
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
var users = {};

socket_io.sockets.on('connection', function (socket) {
	var toSend = {};
	fs.readFile('public/img/12_foot_dinghy.svg', 'utf8' , function (err, data) {
		if (err){ throw err; }
		toSend.ship = data;
		fs.readFile('public/img/ship_ctrl.svg', 'utf8', function(err2, data2){
			if(err2) {throw err2; }
			toSend.forces = data2;
			socket.emit('shipImg', toSend);
		});
	});
	socket.on('message', function (data){
		socket.broadcast.emit('newMessage', data);
	});
	socket.on('newChatter', function (data){
		users[socket.id] = data;
		socket.emit('connected', {'list': users});
		socket.broadcast.emit('connected', {'list': users});
	});
	socket.on('disconnect', function(){
		delete users[socket.id];
		socket.broadcast.emit('connected', {'list': users});
	});
});

