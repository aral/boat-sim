var paApp = angular.module('boatApp', [ 'ngRoute', 'ngSanitize', 'ui.slider',
		'timer' ]);

// Declare app level module which depends on filters, and services
paApp.config([ '$routeProvider', '$locationProvider',
		function($routeProvider, $locationProvider) {

			$routeProvider.otherwise({
				redirectTo : '/'
			});
		} ]);

paApp.run([ '$rootScope', '$location', function($rootScope, $location) {
	console.log("Angular is running");
	console.log($location.path());
} ]);
