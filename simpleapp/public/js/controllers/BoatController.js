'use strict';

angular
	.module('boatApp')
	.controller(
		'BoatController',
		[
			'$scope',
			'$http',
			'$routeParams',
			'$filter',
			'$interval',
			function($scope, $http, $routeParams, $filter,
				$interval) {
			    var simHandle = null;
			    var drawedFirstLine = null;
			    function Statek() {
				this.lastX = 0;
				this.lastY = 0;
				this.x = 0;
				this.y = 0;
				this.w = 0;// polozenie bedace pierwiastkiem
				// sumy kwadratow x i y
				this.przyspieszenie = 0;// przyspieszenie
				this.predkosc = 1; // wartosc
				this.kierunek = 90; // kat alfa wzgledem
				// osi OX
				// -180 do 180 st.
				this.zagiel = 63; // kat obrotu zagla
				// wzgledem
				// osi statku -180 do 180
				// st.
				this.ster = 0; // kat obrotu steru -45
				// do 45
				// st.
				this.mi = 0.015;// staly wsp�czynnik tarcia
				this.masa = 1; // masa statku
				this.wspOporu = 0.18; // wsp. oporu
			    }

			    function Sily() {
				this.aerodynamiczna = 0;
				this.katSilyDoKierunku = 0;// 90 st - kat zagla
				// ??
				this.oporu = 0;
			    }

			    function Wiatr() {// pozorny
				this.predkosc = 5; // predkosc w m/s
				this.kat = 0;// kat wzgledem osi OX
			    }
			    $scope.statek = new Statek();
			    $scope.sily = new Sily();
			    $scope.wiatr = new Wiatr();
			    $scope.sterKat = 0;
			    $scope.masztKat = 0;
			    $scope.symulacjaWtoku = false;
			    $scope.czasStart = 0;
			    $scope.czasEnd = 0;
			    $scope.czas = 0;
			    setInterval(
				    function() {
					if (!$scope.wFx2 || !$scope.sily.x) {
					    $scope.wFx2 = eval($("#wForce")
						    .attr('x2'));
					    $scope.wFy2 = eval($("#wForce")
						    .attr('y2'));
					    $scope.wXy2 = eval($("#xForce")
						    .attr('y2'));
					    $scope.wYx2 = eval($("#yForce")
						    .attr('x2'));
					} else {
					    var forceMult = 10;
					    $("#wForce").attr(
						    'x2',
						    wFx2 + $scope.sily.x
							    * forceMult);
					    $("#wForce").attr(
						    'y2',
						    wFy2 - $scope.sily.y
							    * forceMult);
					    $("#xForce").attr(
						    'y2',
						    wXy2 - $scope.sily.y
							    * forceMult);
					    $("#yForce").attr(
						    'x2',
						    wYx2 + $scope.sily.x
							    * forceMult);
					}
				    }, 1000);

			    setInterval(
				    function() {
					$("#wind")
						.css(
							"-webkit-transform",
							"rotateZ("
								+ -$scope
									.degreeToRad($scope.statek.kierunek)
								+ "rad)");
				    }, 1000);
			    setInterval(
				    function() {
					$("#gSilk")
						.css(
							"-webkit-transform",
							"rotateY("
								+ $scope
									.degreeToRad($scope.statek.zagiel)
								+ "rad)");
				    }, 1000);
			    setInterval(
				    function() {
					$("#_silk_")
						.css(
							"-webkit-transform",
							"rotateZ("
								+ -$scope
									.degreeToRad($scope.statek.zagiel)
								+ "rad)");
				    }, 1000);
			    setInterval(
				    function() {
					// rysowanie polozenia
					var c = document.getElementById("pos");
					var ctx = c.getContext("2d");
					if ($scope.statek.lastX
						|| $scope.statek.lastY) {
					    ctx.moveTo($scope.statek.lastX,
						    $scope.statek.lastY);
					    ctx.lineTo($scope.statek.x,
						    $scope.statek.y);
					    ctx.stroke();
					    $scope.statek.lastX = $scope.statek.x;
					    $scope.statek.lastY = $scope.statek.y;
					} else {
					    $scope.statek.lastX = $scope.statek.x;
					    $scope.statek.lastY = $scope.statek.y;
					}
				    }, 1000);
			    $scope.rownanieRuchu = function(t, vStatku) {
				var clVal = $scope.cl($scope.statek.kierunek,
					$scope.statek.zagiel);
				var cdVal = $scope.cd($scope.statek.kierunek,
					$scope.statek.zagiel);
				var opor = (vStatku[1] == 0 ? 0
					: $scope.statek.mi);
				return ([
					vStatku[1],
					(0.0625 * $scope.wiatr.predkosc
						* $scope.wiatr.predkosc
						* $scope.ct(clVal, cdVal)
						- $scope.statek.wspOporu
						* vStatku[1] * vStatku[1] - opor)
						/ $scope.statek.masa ]);
			    };
			    $scope.symulacja = function() {
				var sol = numeric.dopri(0, 2,
					[ $scope.statek.w,
						$scope.statek.predkosc ],
					$scope.rownanieRuchu);
				$scope.statek.w = sol.y[sol.y.length - 1][0];
				var katB = $scope.katB($scope.statek.kierunek,
					$scope.statek.zagiel);
				$scope.statek.x += Math.cos($scope.degreeToRad($scope.statek.kierunek))
					* $scope.statek.w * Math.cos($scope.degreeToRad($scope.statek.kierunek
					- katB));
				$scope.statek.y += Math.sin($scope.degreeToRad($scope.statek.kierunek))
					* $scope.statek.w
					* Math.sin($scope.degreeToRad($scope.statek.kierunek
								- katB));
				$scope.statek.w = 0;
				$scope.statek.predkosc = sol.y[sol.y.length - 1][1];
				$scope.sily.w = $scope.statek.predkosc;
				$scope.sily.x = $scope.sily.w
					* Math.cos($scope
							.degreeToRad($scope.statek.kierunek
								- $scope
									.katB(
										$scope.statek.kierunek,
										$scope.statek.zagiel)));
				$scope.sily.y = $scope.sily.w
					* Math.sin($scope.degreeToRad($scope.statek.kierunek
								- $scope.katB(
										$scope.statek.kierunek,
										$scope.statek.zagiel)));
				console.log("X: " + $scope.statek.x + ", Y: "
					+ $scope.statek.y + ", V: "
					+ $scope.statek.predkosc
					+ " Sily x,y,w: " + $scope.sily.x + ","
					+ $scope.sily.y + "," + $scope.sily.w);
				$scope.$apply();

			    };
			    $scope.toggleSymulacja = function() {
				$scope.symulacjaWtoku = !$scope.symulacjaWtoku;
				if ($scope.symulacjaWtoku) {
				    $scope.$broadcast('timer-start');
				    $scope.czasStart = new Date();
				    simHandle = setInterval($scope.symulacja,
					    700);
				} else {
				    $scope.$broadcast('timer-stop');
				    $scope.czasEnd = new Date();
				    $scope.lastSimTime = $scope
					    .getTimeDiff($scope.czasStart);
				    clearInterval(simHandle);
				    $scope.resetSim();
				}
			    };
			    $scope.getTimeDiff = function(datetime) {
				var datetime = typeof datetime !== 'undefined' ? datetime
					: "2014-01-01 01:02:03.123456";

				var datetime = new Date(datetime).getTime();
				var now = new Date().getTime();

				if (isNaN(datetime)) {
				    return "";
				}
				if (datetime < now) {
				    var milisec_diff = now - datetime;
				} else {
				    var milisec_diff = datetime - now;
				}

				var days = Math.floor(milisec_diff / 1000 / 60
					/ (60 * 24));

				var date_diff = new Date(milisec_diff);

				return days + " Days "
					+ (date_diff.getHours() - 1)
					+ " Hours " + date_diff.getMinutes()
					+ " Minutes " + date_diff.getSeconds()
					+ " Seconds";
			    };
			    $scope.resetSim = function() {
				$scope.statek = new Statek();
				$scope.sily = new Sily();
				$scope.wiatr = new Wiatr();
				var c = document.getElementById("pos");
				var ctx = c.getContext("2d");
				ctx.clearRect(0, 0, c.width, c.height);
			    };
			    //

			    // funckje pomocnicze
			    $scope.degreeToRad = function(degrees) {
				return degrees * Math.PI / 180;
			    };
			    $scope.radToDegree = function(rads) {
				return rads * 180 / Math.PI;
			    };
			    $scope.cl = function(kierunekKat, zagielKat) {
				var sumaKatow = $scope.katB(
					$scope.statek.kierunek,
					$scope.statek.zagiel) + 9;
				// console.log("CL: " + sumaKatow);
				if (sumaKatow <= 27) {
				    return sumaKatow * 0.0575 - -0.2025;
				} else {
				    return sumaKatow * -0.02 + 1.94;
				}
			    };
			    $scope.cd = function(kierunekKat, zagielKat) {
				var sumaKatow = $scope.katB(
					$scope.statek.kierunek,
					$scope.statek.zagiel) + 9;
				// console.log("CD: " + sumaKatow);
				if (sumaKatow <= 27) {
				    return 0.03 * sumaKatow - 0.11;
				} else {
				    return 0.02 * sumaKatow - 0.14;
				}
			    };
			    $scope.ct = function(alfa, beta) {
				var clVal = $scope.cl(alfa, beta);
				var cdVal = $scope.cd(alfa, beta);
				return Math.sqrt(clVal * clVal + cdVal * cdVal);
			    };
			    $scope.ct2 = function(clVal, cdVal) {
				return Math.sqrt(clVal * clVal + cdVal * cdVal);
			    };
			    $scope.katB = function(kierunekKat, zagielKat) {// kat
				// pomiedzy
				// sila
				// aerodynamiczna
				// a jej
				// kierunkiem zeglugi
				// console.log("Kat B: "
				// + ((kierunekKat - zagielKat) - 9));

				return kierunekKat - zagielKat - 9;
			    };
			    $scope.katB2 = function(wspCl, wspCd) {
				return radToDegree(Math.atan(wspCd / wspCl));
			    };
			} ]);